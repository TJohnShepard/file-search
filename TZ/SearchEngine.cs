﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace TZ
{
    public partial class SearchEngine : Form
    {
        public SearchEngine()
        {
            InitializeComponent();
            textBox1.Text = Properties.Settings.Default.SavedPathInfo;
            textBox2.Text = Properties.Settings.Default.SavedFileInfo;
            textBox3.Text = Properties.Settings.Default.SavedTextInfo;

        }

        string folder;
        string fileName;
        string text;

        int count = 0;

        FileInfo fInfo;

        bool pause = false;
        bool stop = false;

        System.Windows.Forms.Timer countdown;

        public void doSearch(TreeView treeView)
        {
            count++;
            while (pause == true)
                Thread.Sleep(10);

            treeView.PathSeparator = @"\";
            string subPathAgg = string.Empty;
            char pathSeparator = '\\';
            TreeNode lastNode = null;

            foreach (string subPath in fInfo.FullName.Split(pathSeparator))
            {
                subPathAgg += subPath + pathSeparator;
                TreeNode[] nodes = treeView.Nodes.Find(subPathAgg, true);
                if (nodes.Length == 0)
                    if (lastNode == null)
                        treeView.Invoke(new Action(delegate () { lastNode = treeView.Nodes.Add(subPathAgg, subPath); }));
                    else
                        treeView.Invoke(new Action(delegate () { lastNode = lastNode.Nodes.Add(subPathAgg, subPath); }));
                else
                    lastNode = nodes[0];
            }
            treeView.Invoke(new Action(delegate () { treeView.ExpandAll(); treeView.Update(); }));
            label7.Invoke(new Action(delegate () { label7.Text = count.ToString(); label7.Update(); }));
            label6.Invoke(new Action(delegate () { label6.Text = fInfo.FullName; label6.Update(); }));

        }
        public void Search()
        {

                foreach (string result in Directory.EnumerateFiles(folder, fileName, SearchOption.AllDirectories))
                {
                        fInfo = new FileInfo(result);
                    if (text == string.Empty)
                        {
                            doSearch(treeView1);
                        }
                    else
                    {

                        var lines = File.ReadAllLines(fInfo.FullName);
                        foreach (var line in lines)
                        {
                            if (line.Contains(text))
                            {
                                doSearch(treeView1);
                                break;
                            }
                        }
                    }
                if (stop == true)
                {
                    break;
                }
            }
            
            
            countdown.Stop();
            button1.Invoke(new Action(delegate () { button1.Text = "Search"; } ));
            count = 0;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            textBox1.Text = fbd.SelectedPath;
        }
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void countdown_tick(object sender, EventArgs e)
        {
            if (pause == false)
            {
                label8.Text = (int.Parse(label8.Text) + 1).ToString();
                label8.Update();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "Stop")
            {
                stop = true;
                pause = false;
                button2.Text = "Pause";
                button1.Text = "Search";
            }
            else
            {
                pause = false;
                button2.Text = "Pause";
                stop = false;

                folder = textBox1.Text;
                fileName = '*' + textBox2.Text + '*';
                text = textBox3.Text;

                Properties.Settings.Default.SavedPathInfo = folder;
                Properties.Settings.Default.SavedFileInfo = fileName.Trim('*');
                Properties.Settings.Default.SavedTextInfo = text;
                Properties.Settings.Default.Save();

                treeView1.Nodes.Clear();
                button1.Text = "Stop";
                label7.Text = string.Empty;
                label6.Text = string.Empty;

                countdown = new System.Windows.Forms.Timer();
                label8.Text = "0";
                countdown.Tick += new EventHandler(countdown_tick);
                countdown.Interval = 1000;
                countdown.Start();

                Thread t = new Thread(new ThreadStart(Search));
                t.Start();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (pause == false)
            {
                pause = true;
                button2.Text = "Unpause";
            }
            else
            {
                pause = false;
                button2.Text = "Pause";
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            treeView1.CollapseAll();
        }
    }
}
